var inquirer = require('inquirer');

function validateInt(value) {
  if (Number.isInteger(parseInt(value))) {
      return true;
  } else {
      return "Please enter an Integer";
  }
}

function validateType(value) {
  value = value.toLowerCase();
  switch (value) {
    case 's':
    case 'i':
    case 'str':
    case 'int':
    case 'string':
    case 'integer':
      return true;
    default:
      return "Please enter 'string' or 'integer'";
  }
}

let questions = [
{
  type: 'input',
  name: 'num1',
  message: "What's the first number",
  validate: validateInt
},
{
  type: 'input',
  name: 'num2',
  message: "What's the second number",
  validate: validateInt
},
{
    type: 'input',
    name: 'type',
    message: "Are they strings or integers",
    validate: validateType
}];

inquirer
  .prompt(questions)
  .then(answers => {
    
    console.log(answers);

  });